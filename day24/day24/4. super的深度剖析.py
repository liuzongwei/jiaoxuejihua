# 严格按照mro的执行顺序去执行

# class A:
#     def f1(self):
#         print('in A f1')
#
#     def f2(self):
#         print('in A f2')
#
#
# class Foo(A):
#     def f1(self):
#         super(Foo,self).f2()  # 按照self对象从属于类的mro的顺序,执行Foo类的下一个类.
#         print('in A Foo')

#
# obj = Foo()
# obj.f1()




# class A:
#     def f1(self):
#         print('in A')
#
# class Foo(A):
#     def f1(self):
#         super(Foo,self).f1()
#         print('in Foo')
#
# class Bar(A):
#     def f1(self):
#         print('in Bar')
#
# class Info(Foo, Bar):
#     def f1(self):
#         super(Info,self).f1()  #  按照self对象从属于类的mro的顺序,执行Info类的下一个类.
#         print('in Info f1')
# print(Info.mro())
# # [Info'>, Foo'>, Bar'>, A',  'object'>]
# obj = Info()
# obj.f1()



class A:
    def f1(self):
        print('in A')

class Foo(A):
    def f1(self):
        super().f1()
        print('in Foo')

class Bar(A):
    def f1(self):
        print('in Bar')  # 1

class Info(Foo,Bar):
    def f1(self):
        super(Foo,self).f1()
        print('in Info f1')  # 2
# [Info Foo Bar A]
obj = Info()
obj.f1()

