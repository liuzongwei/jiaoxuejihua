# 1，验证服务端缓冲区数据没有取完，又执行了recv执行，recv会继续取值。
# import socket
# phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# phone.connect(('127.0.0.1',8080))
# phone.send('hello'.encode('utf-8'))
# phone.close()



# 2，验证服务端缓冲区取完了，又执行了recv执行，此时客户端20秒内不关闭的前提下，recv处于阻塞状态。
# import socket
# import time
# phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# phone.connect(('127.0.0.1',8080))
# phone.send('hello'.encode('utf-8'))
# time.sleep(20)
#
# phone.close()

# # 3，验证服务端缓冲区取完了，又执行了recv执行，此时客户端处于关闭状态，则recv会取到空字符串。
# import socket
# import time
# phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# phone.connect(('127.0.0.1',8080))
# phone.send('hello'.encode('utf-8'))
# phone.close()