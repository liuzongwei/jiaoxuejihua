from multiprocessing import Process
import time
import os
def task(name):
    print(f'{name} is running')
    print(f'主进程: {os.getppid()}')
    print(f'子进程: {os.getpid()}')
    time.sleep(50)
    print(f'{name} is gone')



if __name__ == '__main__':
    # 在windows环境下, 开启进程必须在 __name__ == '__main__' 下面
    p = Process(target=task,args=('常鑫',))  # 创建一个进程对象
    p.start()
    print('==主开始')

