class Human:
    """
    类的具体结构
    """
    # 第一部分:静态属性
    mind = '有思想'   # 类的属性  (静态属性, 静态字段)
    language = '使用语言'

    # 第二部分: 动态方法
    def work(self):
        print('人类都会工作')

    def eat(self):
        print('人类都需要吃饭')


