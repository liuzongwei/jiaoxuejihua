# 1.内置函数一
# eval()
# exec()
# all()
# any()
# hash()
# chr
# ord
# bin
# hex
# oct
# callable()
# divmod()
# pow()
# round
# float
# complex
# id
# int
# str
# list
# tuple
# dict
# set
# globals()
# locals()


# 2.生成器
# 生成器的本质就是一个迭代器
# 生成器和迭代器的区别:
# 迭代器是python自带的
# 生成器是程序员自己写的

# def func():
#     print(1)
#     yield 123
# g = func() # 产生一个生成器
# print(next(g))

# def func():
#     print(1)
#     yield 123
#     print(2)
#     yield 234
#     print(3)
#     yield 345
#     yield 456
#
# g = func() # 产生一个生成器
# print(g.__next__())
# print(g.__next__())
# print(g.__next__())
# print(g.__next__())
#
# for i in g:
#     print(i)

# def func():
#     lst = ["牛羊配","卫龙","老干妈"]
#     yield lst
# print(func().__next__())


# def func():
#     lst = ["牛羊配","卫龙","老干妈"]
#     yield from lst
# g = func()
# print(next(g))
# print(next(g))
# print(next(g))


# 3.推导式
# list [变量 for循环 加工条件]
# dict {键:值 for循环 加工条件}
# set  {变量 for循环 加工条件}
# 生成器表达式 (变量 for循环 加工条件)