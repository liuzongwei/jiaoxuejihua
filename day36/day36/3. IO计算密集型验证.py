from threading import Thread
from multiprocessing import Process
import time
import random
# # 计算密集型: 单个进程的多线程并发 vs 多个进程的并发并行
#
# def task():
#     count = 0
#     for i in range(10000000):
#         count += 1
#
#
# if __name__ == '__main__':
#
#     # 多进程的并发,并行
#     # start_time = time.time()
#     # l1 = []
#     # for i in range(4):
#     #     p = Process(target=task,)
#     #     l1.append(p)
#     #     p.start()
#     #
#     # for p in l1:
#     #     p.join()
#     #
#     # print(f'执行效率:{time.time()- start_time}')  # 3.1402080059051514
#
#     # 多线程的并发
#     # start_time = time.time()
#     # l1 = []
#     # for i in range(4):
#     #     p = Thread(target=task,)
#     #     l1.append(p)
#     #     p.start()
#     #
#     # for p in l1:
#     #     p.join()
#     #
#     # print(f'执行效率:{time.time()- start_time}')  # 4.5913777351379395

# 总结: 计算密集型: 多进程的并发并行效率高.


# IO密集型: 单个进程的多线程并发 vs 多个进程的并发并行

def task():
    count = 0
    time.sleep(random.randint(1,3))
    count += 1

# if __name__ == '__main__':

# 多进程的并发,并行
#     start_time = time.time()
#     l1 = []
#     for i in range(50):
#         p = Process(target=task,)
#         l1.append(p)
#         p.start()
#
#     for p in l1:
#         p.join()
#
#     print(f'执行效率:{time.time()- start_time}')  #  8.000000000

# 多线程的并发
#     start_time = time.time()
#     l1 = []
#     for i in range(50):
#         p = Thread(target=task,)
#         l1.append(p)
#         p.start()
#
#     for p in l1:
#         p.join()
#
#     print(f'执行效率:{time.time()- start_time}')  # 3.0294392108917236

# 对于IO密集型: 单个进程的多线程的并发效率高.