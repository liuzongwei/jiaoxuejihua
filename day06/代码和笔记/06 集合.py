# 集合 -- set
# 没有值得字典  无序 -- 不支持索引
# 天然去重 *****

# 定义集合
# s = {1,"alex",False,(1,2,3),12,1,12,4,6,32,2,4}
# print(s)

# 面试题:
# lst = [1,2,1,2,4,2,45,3,2,45,2345,]
# print(list(set(lst)))

# s = {1,2,3,4}

# 增:
# s.add("67")  #只能添加一个
# print(s)
# s.update("今天")  # 迭代添加
# print(s)

# 删:
# print(s.pop())  # pop有返回值
# print(s)
# s.remove(3)     # 指定元素删除
# print(s)
# s.clear()         # 清空   -- set() 空集合
# print(s)

# 改:
# 先删在加

# 查:
# for i in {1,2,3}:
#     print(i)

# 其他操作:
# s1 = {1,2,3,4,5,6,7}
# s2 = {5,6,7,1}
# print(s1 & s2)  # 交集
# print(s1 | s2)  # 并集
# print(s1 - s2)  # 差集
# print(s1 ^ s2)  # 反交集
# print(s1 > s2)  # 父集(超集)
# print(s1 < s2)  # 子集

# print(frozenset(s1))  # 冻结集合 更不常用
# dic = {frozenset(s1):1}
# print(dic)