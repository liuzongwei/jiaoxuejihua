# 1.字典 -- dict

# 定义一个字典:
# dic = {"常大哥":98,"文虎":100}
# 常大哥 是键  98 是值

# 键:是可哈希(不可变的数据类型) 键是唯一的
# 值:是任意的

# 增:
# dic["键"] = "值"
# setdefault  #有就不添加,没有才添加 (进行查询)

#删:
# del dic["键"]
# dic.pop("键")
# dic.clear()  # 清空

#改:
# dic["键"] = "值"
# dic.update({"键":"值"})

#查:
# for i in dic: #获取字典中所有键值对的键
# dic.get("键") # 通过键获取值
# dic["键"]   # 通过键进行查询
# dic.setdefault("键")  通过键进行查询

# dic.keys()  # 获取所有的键
# dic.values() # 获取所有的值
# dic.items()     # 获取所有的键和值

# for i in dic:
#     print(i,dic[i])  # 获取所有的键和值

# 解构
# a = 10
# b = 20
# a,b = b,a  # 一行进行数据交换  #### 面试会问

# a,b = 12,35
# print(a,b)   # 一一对应


# for k,v in dic.items(): # [("alex","吹牛皮"),("wusir","活好")]
#     print(k,v)

# "拼接符号".join("可迭代对象") -- ["alex",123]

# 字典的嵌套

# dic = {"alex":{"wusir":{"活好","气大"}}}
# 查找嵌套的时候通过键一层一层进行查找