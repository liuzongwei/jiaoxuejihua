# join: 阻塞 告知主线程要等待我子线程执行完毕之后再执行主线程

# from threading import Thread
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(1)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#     start_time = time.time()
#     t1 = Thread(target=task,args=('海狗',))
#     t2 = Thread(target=task,args=('海狗1',))
#     t3 = Thread(target=task,args=('海狗2',))
#
#     t1.start()
#     t1.join()
#     t2.start()
#     t2.join()
#     t3.start()
#     t3.join()
#
#     print(f'===主线程{time.time() - start_time}')  # 线程是没有主次之分的.


# 守护线程

# 回忆一下守护进程

# from multiprocessing import Process
# import time
#
#
# def foo():
#     print(123)
#     time.sleep(1)
#     print("end123")
#
#
# def bar():
#     print(456)
#     time.sleep(2)
#     print("end456")
#
# if __name__ == '__main__':
#
#     p1 = Process(target=foo,)
#     p2 = Process(target=bar,)
#
#     p1.daemon = True
#     p1.start()
#     p2.start()
#     print('====主')



# 守护线程

# from threading import Thread
# import time
#
#
# def sayhi(name):
#     print('你滚!')
#     time.sleep(2)
#     print('%s say hello' %name)
#
# if __name__ == '__main__':
#     t = Thread(target=sayhi,args=('egon',))
#     # t.setDaemon(True) #必须在t.start()之前设置
#     t.daemon = True
#     t.start()  # 线程的开启速度要跟进程开很多
#
#     print('主线程')



# from threading import Thread
# import time
#
# def foo():
#     print(123)  # 1
#     time.sleep(1)
#     print("end123")  # 4
#
# def bar():
#     print(456)  # 2
#     time.sleep(3)
#     print("end456")  # 5
#
#
# t1=Thread(target=foo)
# t2=Thread(target=bar)
#
# t1.daemon=True
# t1.start()
# t2.start()
# print("main-------")  # 3


# 主线程什么时候结束???
# 守护线程 等待非守护子线程以及主线程结束之后,结束.
# from threading import Thread
# import time
#
# def foo():
#     print(123)  # 1
#     time.sleep(3)
#     print("end123")  # 4
#
# def bar():
#     print(456)  # 2
#     time.sleep(1)
#     print("end456")  # 5
#
#
# t1=Thread(target=foo)
# t2=Thread(target=bar)
#
# t1.daemon=True
# t1.start()
# t2.start()
# print("main-------")  # 3
'''
123
456
main-------
end123
end456


123
456
main
end456

main
456
end456

456
123
main
end456


'''


# from threading import Thread
# import time
#
# def foo():
#     print(123)
#     time.sleep(3)
#     print("end123")
#
# def bar():
#     print(456)
#     time.sleep(1)
#     print("end456")
#
#
# t1=Thread(target=foo)
# t2=Thread(target=bar)
#
# t1.daemon=True
# t1.start()
# t2.start()
# print("main-------")
