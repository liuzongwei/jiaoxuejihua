# from threading import Thread
# import time
# import random
# x = 100
#
# def task():
#     time.sleep(random.randint(1,2))
#     global x
#     temp = x
#     time.sleep(random.randint(1, 3))
#     temp = temp - 1
#     x = temp
#
#
# if __name__ == '__main__':
#     l1 = []
#     for i in range(100):
#         t = Thread(target=task)
#         l1.append(t)
#         t.start()
#
#     for i in l1:
#         i.join()
#     print(f'主线程{x}')

# 多个任务公抢一个数据,保证数据的安全的目的,要让其串行


from threading import Thread
from threading import Lock
import time
import random
x = 100

def task(lock):

    lock.acquire()
    # time.sleep(random.randint(1,2))
    global x
    temp = x
    time.sleep(0.01)
    temp = temp - 1
    x = temp
    lock.release()


if __name__ == '__main__':
    mutex = Lock()
    l1 = []
    for i in range(100):
        t = Thread(target=task,args=(mutex,))
        l1.append(t)
        t.start()

    time.sleep(3)
    print(f'主线程{x}')