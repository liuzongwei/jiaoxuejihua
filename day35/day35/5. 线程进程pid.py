# from multiprocessing import Process
# import time
# import os
# def task(name):
#     print(f'子进程: {os.getpid()}')
#     print(f'主进程: {os.getppid()}')
#
#
# if __name__ == '__main__':
#
#     p1 = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p2 = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p1.start()
#     p2.start()
#     print(f'==主{os.getpid()}')



# 线程:



from threading import Thread
import os

def task():
    print(os.getpid())

if __name__ == '__main__':

    t1 = Thread(target=task)
    t2 = Thread(target=task)
    t1.start()
    t2.start()
    print(f'===主线程{os.getpid()}')

