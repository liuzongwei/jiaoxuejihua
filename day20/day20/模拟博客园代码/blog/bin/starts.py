# run() # 找不到名字
# 找到src.py文件的run名字

# from src import run
# run()
# sys.path 到底是什么?

# import time
# import t1
# import sys
# t1.func()
'''
sys模块内置模块,文件运行时,sys内置模块就会将一些模块 自动  加载到内存. 内置模块. time,json pickle等等.以及当前目录的.
如何引用到一个模块最本质的原因在于这个模块名称空间在不在内存.
如果直接引用不到一个模块,他必定不是内置或者当前目录下的py文件.
所以,我们要手动将其添加到内存. 
sys.path.append()就是手动的将一些模块添加到内存,添加完毕之后,就可以直接引用了.
'''
# import sys
# # 版本二: 繁琐,我要将整个项目作为根目录,添加到内存中.
# sys.path.append(r'F:\s24\day20\模拟博客园代码\blog\core')
# sys.path.append(r'F:\s24\day20\模拟博客园代码\blog\conf')
# sys.path.append(r'F:\s24\day20\模拟博客园代码\blog\db')
# from src import run
# run()


# 版本三: 你把项目路径写死了,所有的开发人员如果共同开发这个项目,必须都得按照这个目录结构去构建.
# import sys
# sys.path.append(r'F:\s24\day20\模拟博客园代码\blog')
# from core.src import run
# run()


# 版本四:
import sys
import os

# print(__file__)
# print(os.path.dirname(os.path.dirname(__file__)))
# sys.path.append(r'F:\s24\day20\模拟博客园代码\blog')
# from core.src import run
# run()

BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
from core.src import run
if __name__ == '__main__':

    run()

