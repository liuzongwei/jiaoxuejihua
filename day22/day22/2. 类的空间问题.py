# class Human:
#
#     mind = '有思想的'
#
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def eat(self,argv):
#         # self.hobby = '女'
#         Human.body = argv
#         print('吃饭')
#
#
# sun = Human('孙宇', 18)
# # 何处可以添加对象属性?
# # 在类的__init__可以添加, 在类的方法中也可以添加,在类的外部也可以添加
# # sun.eat()
# # print(sun.__dict__)
# # sun.weight = 130
# # print(sun.__dict__)
#
# # 何处添加类的属性?
# # 类的内部
# # sun.eat('有头有脸')
# # 类的外部
# # Human.body = '有头四肢'
# # print(Human.__dict__)
#
# # 类与对象的关系
# # sun.mind = '无脑'
# # print(sun.mind)
# # print(Human.mind)
#
# del sun.mind

# 查询顺序:  对象.名字:   对象空间  ------>

