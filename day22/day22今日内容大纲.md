1. 今日内容大纲

   远离"垃圾人" 

   1. 从空间角度研究类

      1. 何处添加对象属性
      2. 何处添加类的属性
      3. 对象空间与类空间的关系

      ![1564972633128](D:\课上笔记\assets\1564972633128.png)

      对象空间与类空间有相同的名字, 对象. 肯定先从对象空间查找.

      查询顺序:  

      ​	对象.名字:   对象空间  ______类对象指针_______--> 类空间 ---> 父类空间

      ​	类名.名字: 类空间 -----> 父类空间

      

   2. 类与类之间的关系

      1. 依赖关系

         将一个类的类名或者对象传入另一个类的方法中.

         ```
         #1. 依赖关系: 主从之分.
         class Elephant:
         
             def __init__(self,name):
                 self.name = name
         
             def open(self,obj):
                 print(f'{self.name} 默念三声: 3,2,1 开门')
                 obj.be_open()
         
             def close(self):
                 print(f'{self.name} 默念三声: 3,2,1 关门')
         
         
         class Refrigerator:
         
             def __init__(self, name):
                 self.name = name
         
             def be_open(self):
                 print(f'{self.name}冰箱 被打开了')
         
             def be_close(self):
                 print(f'{self.name}冰箱 被关闭了')
         
         # 依赖关系: 将一个类的类名或者对象传入另一个类的方法中.
         
         qiqi = Elephant('奇奇')
         haier = Refrigerator('海尔')
         qiqi.open(haier)
         
         ```
      
         
      
      2. 组合关系
      
         给一个类的对象封装一个属性,此属性为另一个类的对象
      
         ![1564976408250](D:\课上笔记\assets\1564976408250.png)
      
         ```
         # 2. 组合关系
         
         # class Boy:
         #
         #     def __init__(self, name, girlfriend=None):
         #         self.name = name
         #         self.girlfriend = girlfriend
         #
         #     def have_a_diner(self):
         #         if self.girlfriend:
         #             print(f'{self.name}请他的女朋友{self.girlfriend}一起烛光晚餐')
         #         else:
         #             print('单身狗,吃什么吃')
         
         # liye = Boy('李业')
         # liye.have_a_diner()
         
         # liye = Boy('李业')
         # # 只是给李业封装了一个属性:girlfriend 为一个字符串的数据
         # liye.girlfriend = '乔碧萝'
         # liye.have_a_diner()
         
         
         
         # class Boy:
         #
         #     def __init__(self, name, girlfriend=None):
         #         self.name = name
         #         self.girlfriend = girlfriend
         #
         #     def have_a_diner(self):
         #         if self.girlfriend:
         #             print(f'{self.name}请他的{self.girlfriend.age}岁的,{self.girlfriend.body}的女朋友{self.girlfriend.name}一起烛光晚餐')
         #         else:
         #             print('单身狗,吃什么吃')
         #
         #     def girl_skill(self):
         #         print(f'{self.name}的女朋友的技能:')
         #         self.girlfriend.skill()
         #
         # class Girl:
         #
         #     def __init__(self,name,age,body):
         #         self.name = name
         #         self.age = age
         #         self.body=body
         #
         #     def skill(self):
         #         print(f'{self.name} 会用萝莉音直播')
         #
         #
         # liye = Boy('李业')
         # qiao = Girl('乔碧萝', 58, '小钢炮')
         # liye.girlfriend = qiao
         # # liye.have_a_diner()
         # liye.girl_skill()
         
         
         
         # class GameRole:
         #
         #     def __init__(self, name, ad, hp):
         #         self.name = name
         #         self.ad = ad
         #         self.hp = hp
         #
         #     def attack(self, p1):
         #         p1.hp = p1.hp - self.ad
         #         print(f"{self.name}攻击{p1.name},谁掉了{self.ad}血,  还剩{p1.hp}血")
         #         print(f'{p1.name}的血量{p1.hp}')
         #
         # class Weapon:
         #
         #     def __init__(self,name,ad):
         #         self.name = name
         #         self.ad = ad
         #
         #     def weapon_attack(self, p1, p2):  # 依赖关系
         #
         #         p2.hp = p2.hp - self.ad
         #         print(f'{p1.name}利用{self.name}给了{p2.name}一下子,{p2.name}掉了{self.ad}血,还剩{p2.hp}血')
         #
         #
         #
         # gailun = GameRole('盖伦', 10, 100)
         # xin = GameRole('菊花信', 20, 80)
         # Sword = Weapon('大宝剑',15)
         # Musket = Weapon('长缨枪',30)
         
         # 盖伦利用大宝剑给赵信一下子
         # Sword.weapon_attack(gailun, xin)
         # 1. 功能虽然实现了,但是逻辑上不合理,应该是人物对象调用方法.主体.
         # 2. 游戏人物本身就应该绑定武器属性.
         
         
         
         class GameRole:
         
             def __init__(self, name, ad, hp):
                 self.name = name
                 self.ad = ad
                 self.hp = hp
         
             def attack(self, p1):
                 p1.hp = p1.hp - self.ad
                 print(f"{self.name}攻击{p1.name},谁掉了{self.ad}血,  还剩{p1.hp}血")
                 print(f'{p1.name}的血量{p1.hp}')
         
             def equipment_wea(self, wea):
                 self.weapon = wea  # 组合关系
         
         class Weapon:
         
             def __init__(self,name,ad):
                 self.name = name
                 self.ad = ad
         
             def weapon_attack(self, p1, p2):  # 依赖关系
                 print(f'self---->: {self}')  # self 永远默认接受本类实例化对象
                 p2.hp = p2.hp - self.ad
                 print(f'{p1.name}利用{self.name}给了{p2.name}一下子,{p2.name}掉了{self.ad}血,还剩{p2.hp}血')
         
         
         
         gailun = GameRole('盖伦', 10, 100)
         xin = GameRole('菊花信', 20, 80)
         Sword = Weapon('大宝剑',15)
         Musket = Weapon('长缨枪',30)
         
         # 给游戏人物封装武器属性
         gailun.equipment_wea(Sword)
         # print(gailun.__dict__)
         # gailun.weapon.weapon_attack()
         # print(f'gailun:  ---> {gailun}')
         # print(f'Sord:  ---> {Sword}')
         gailun.weapon.weapon_attack(gailun,xin)
         
         ```
      
         
      
      3. 继承关系

2. 昨日内容回顾以及作业讲解

3. 今日内容

   

4. 今日总结

   主要明白: 依赖和组合的关系,以及会熟练地使用.

   依赖与组合让类与类产生关系, 增强耦合性.

   

5. 明日预习

   继承.

   