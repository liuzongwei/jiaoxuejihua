

# 昨日内容回顾

## orm单表操作  对象关系映射(object relational mapping)

orm语句 -- sql -- 调用pymysql客户端发送sql -- mysql服务端接收到指令并执行

### orm介绍

```
django 连接mysql
1 settings配置文件中
	DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'orm02',
            'USER':'root',
            'PASSWORD':'666',
            'HOST':'127.0.0.1',
            'PORT':3306,
        }
    }
2 项目文件夹下的init文件中写上下面内容,用pymysql替换mysqldb
	import pymysql
	pymysql.install_as_MySQLdb()

3 models文件中创建一个类

# class UserInfo(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=10)
#     bday = models.DateField()
#     checked = models.BooleanField()
4 执行数据库同步指令,添加字段的时候别忘了,该字段不能为空,所有要么给默认值,要么设置它允许为空 null=True
# python manage.py makemigrations
# python manage.py migrate

5 创建记录(实例一个对象,调用save方法)
def query(request):
    # 创建一条记录,增

    new_obj = models.UserInfo(
        id=2,
        name='子文',
        bday='2019-09-27',
        checked=1,

    )
    new_obj.save()  #翻译成sql语句,然后调用pymysql,发送给服务端  insert into app01_userinfo values(2,'子文','2019-09-27',1)

    return HttpResponse('xxx')
```



增:

```
方式1:
    new_obj = models.UserInfo(
        id=2,
        name='子文',
        bday='2019-09-27',
        checked=1,

    )
    new_obj.save() 
方式2:
	# ret 是创建的新的记录的model对象(重点)
	ret = models.UserInfo.objects.create(
        name='卫贺',
        bday='2019-08-07',
        checked=0
    )

    print(ret)  #UserInfo object  卫贺
    print(ret.name)  #UserInfo object
    print(ret.bday)  #UserInfo object


```

### 时间问题

```
models.UserInfo.objects.create(
        name='杨泽涛2',
        bday=current_date,
        # now=current_date,  直接插入时间没有时区问题
        checked=0
    )
	但是如果让这个字段自动来插入时间,就会有时区的问题,auto_now_add创建记录时自动添加当前创建记录时的时间,存在时区问题
now = models.DateTimeField(auto_now_add=True,null=True)
解决方法:
    settings配置文件中将USE_TZ的值改为False
    # USE_TZ = True
    USE_TZ = False  # 告诉mysql存储时间时按照当地时间来寸,不要用utc时间
使用pycharm的数据库客户端的时候,时区问题要注意
```



删

```
简单查询:filter()  -- 结果是queryset类型的数据里面是一个个的model对象,类似于列表
	models.UserInfo.objects.filter(id=7).delete()  #queryset对象调用
	models.UserInfo.objects.filter(id=7)[0].delete()  #model对象调用
```

改

```
方式1:update
    # models.UserInfo.objects.filter(id=2).update(
    #     name='篮子文',
    #     checked = 0,
    #
    # )
    # 错误示例,model对象不能调用update方法
    # models.UserInfo.objects.filter(id=2)[0].update(
    #     name='加篮子+2',
    #     # checked = 0,
    # )
方式2 
    ret = models.UserInfo.objects.filter(id=2)[0]
    ret.name = '加篮子+2'
    ret.checked = 1
    ret.save()
    

更新时的auto_now参数
	# 更新记录时,自动更新时间,创建新纪录时也会帮你自动添加创建时的时间,但是在更新时只有使用save方法的方式2的形式更新才能自动更新时间,有缺陷,放弃
    now2 = models.DateTimeField(auto_now=True,null=True)
```



# 今日内容

### 批量插入(bulk_create)  

```
    # bulk_create
    obj_list = []
    for i in range(20):
        obj = models.Book(
            title=f'金瓶梅{i}',
            price=20+i,
            publish_date=f'2019-09-{i+1}',
            publish='24期出版社'
        )
        obj_list.append(obj)

    models.Book.objects.bulk_create(obj_list)  #批量创建
```



```
request.POST --  querydict类型 {'title': ['asdf '], 'price': ['212'], 'publish_date': ['2019-09-12'], 'publish': ['asdf ']}
data = request.POST.dict() -- 能够将querydict转换为普通的python字典格式

创建数据
	models.Book.objects.create(
            # title=title,
            # price=price,
            # publish_date=publish_date,
            # publish=publish
            **data
        )
```



### 查询api

```
all()  结果为queryset类型

filter 条件查询
	ret = models.Book.objects.filter(title='金瓶梅7',publish='24期出版社') #and多条件查询
	查询条件不能匹配到数据时,不会报错,返回一个空的queryset,<QuerySet []>,如果没有写查询条件会获取所有数据,queryset类型的数据还能够继续调用fitler方法
	
get()
	ret = models.Book.objects.get() #得到的是一个model对象,有且只能有一个
	1. 查不到数据会报错 :Book matching query does not exist.
	2. 超过一个就报错 :returned more than one Book -- it returned 13!

exclude()  #排除
	1.object能够调用,models.Book.objects.exclude(title__startswith='金瓶梅')	
	2.queryset类型数据能够调用, 	models.Book.objects.all().exclude(title__startswith='金瓶梅')

order_by()排序
	models.Book.objects.all().order_by('-price','id')  #orderby price desc,id asc;

reverse() 反转
	models.Book.objects.all().order_by('id').reverse()  #数据排序之后才能反转
	
count() 计数,统计返回结果的数量
	models.Book.objects.all().count() 
first() 返回第一条数据,结果是model对象类型
last()  返回最后一条数据,结果是model对象类型
	# ret = models.Book.objects.all().first()
	ret = models.Book.objects.all().last()
	
exists() 判断返回结果集是不是有数据	
	models.Book.objects.filter(id=9999).exists() #有结果就是True,没有结果就是False

values(返回的queryset,里面是字典类型数据)
values_list(返回的queryset,里面是数组类型数据)
    ret = models.Book.objects.filter(id=9).values('title','price') 
    # ret = models.Book.objects.all().values_list('title','price')
    # ret = models.Book.objects.all().values()
    # ret = models.Book.objects.values()  #调用values或者values_list的是objects控制器,那么返回所有数据 

distinct() 去重,配置values和values_list来使用
	models.Book.objects.all().values('publish').distinct()
```



filter双下划线查询

```

    # ret = models.Book.objects.all().values('publish').distinct()
    # ret = models.Book.objects.filter(price__gt=35)  #大于
    # ret = models.Book.objects.filter(price__gte=35) # 大于等于
    # ret = models.Book.objects.filter(price__lt=35) # 小于等于
    # ret = models.Book.objects.filter(price__lte=35) # 小于等于
    # ret = models.Book.objects.filter(price__range=[35,38]) # 大于等35,小于等于38   # where price between 35 and 38
    # ret = models.Book.objects.filter(title__contains='金瓶梅') # 字段数据中包含这个字符串的数据都要
    # ret = models.Book.objects.filter(title__contains='金瓶梅')
    # ret = models.Book.objects.filter(title__icontains="python")  # 不区分大小写
    # from app01.models import Book
    # ret = models.Book.objects.filter(title__icontains="python")  # 不区分大小写
    # ret = models.Book.objects.filter(title__startswith="py")  # 以什么开头，istartswith  不区分大小写
    # ret = models.Book.objects.filter(publish_date='2019-09-15')

某年某月某日:
	ret = models.Book.objects.filter(publish_date__year='2018') 
	ret = models.Book.objects.filter(publish_date__year__gt='2018')#2018写数字也可以
    ret = models.Book.objects.filter(publish_date__year='2019',publish_date__month='8',publish_date__day='1')
    
找字段数据为空的双下滑线
	models.Book.objects.filter(publish_date__isnull=True) #这个字段值为空的那些数据
```



### 表结构

```
from django.db import models

# Create your models here.

class Author(models.Model):
    """
    作者表
    """
    name=models.CharField( max_length=32)
    age=models.IntegerField()
    # authorDetail=models.OneToOneField(to="AuthorDetail",to_field="nid",on_delete=models.CASCADE)  #
    au=models.OneToOneField("AuthorDetail",on_delete=models.CASCADE)

class AuthorDetail(models.Model):
    """
    作者详细信息表
    """
    birthday=models.DateField()
    telephone=models.CharField(max_length=11)
    addr=models.CharField(max_length=64)
    # class Meta:
        # db_table='authordetail' #指定表名
        # ordering = ['-id',]
class Publish(models.Model):
    """
    出版社表
    """
    name=models.CharField( max_length=32)
    city=models.CharField( max_length=32)

class Book(models.Model):
    """
    书籍表
    """
    title = models.CharField( max_length=32)
    publishDate=models.DateField()
    price=models.DecimalField(max_digits=5,decimal_places=2)
    publishs=models.ForeignKey(to="Publish",on_delete=models.CASCADE,)
    authors=models.ManyToManyField('Author',)




```























































































































