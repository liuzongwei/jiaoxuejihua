"""
4.请用代码实现：
li = ["alex", "wusir", "taibai"]
利用下划线将列表的每一个元素拼接成字符串"alex_wusir_taibai"
"""

li = ["alex", "wusir", "taibai"]
# s = "" # "alex_wusir_taibai_"       #   ""+"alex"+"_ = "alex_"   "alex_"+"wusir"+"_"  "alex_wusir_" +"taibai"+"_"
# for i in li:
#     s = s + i + "_"
# print(s[:-1])

# print("_".join(li))  # 拼接的符号.join(可迭代对象)

# 错误的示范
# print(str.join(li))

"""
9.利用for循环和range从100~10，倒序将所有的偶数添加到一个新列表中，然后在对列表的元素进行筛选，将能被4整除的数留下来。
"""
# lst = []
# for i in range(100,9,-2):
#     lst.append(i)
#
# for j in lst:
#     if j % 4 != 0:
#         lst.remove(j)
# print(lst)

"""
10.利用for循环和range，将1-30的数字中能被3整除的数改成* 依次添加到的列表当中
"""
# lst = []
# for i in range(1,31):
#     if i % 3 == 0:
#         lst.append("*")
#     else:
#         lst.append(i)
# print(lst)

"""
11.查找列表li中的元素，移除每个元素的空格，并找出以"A"或者"a"开头，
并以"c"结尾的所有元素，并添加到一个新列表中,最后循环打印这个新列表。
"""
# li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", " aqc"]
# lst = []
# for i in li:
#     em = i.strip()
#     if (em.startswith("A") or em.startswith("a")) and em.endswith("c"):
#         lst.append(em)
# print(lst)

"""
12.开发敏感词语过滤程序，提示用户输入评论内容，如果用户输入的内容中包含特殊的字符：
敏感词列表 li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
则将用户输入的内容中的敏感词汇替换成等长度的*（苍老师就替换***），
并添加到一个列表中；如果用户输入的内容没有敏感词汇，则直接添加到上述的列表中。
"""
li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
# len()
# in
# if
# input()
# replace

# lst = []
# content = input("请输入内容:") # "苍老师真是厉害,教育了一代又一代,武藤兰也是一个狠人啊"
# for i in li:
#     if i in content:
#         content = content.replace(i,len(i) * "*")  # em = "苍老师真是厉害,教育了一代又一代,***也是一个狠人啊"
          # 有点坑
# lst.append(content)
# print(lst)

"""
13.有如下列表（选做题）

循环打印列表中的每个元素，遇到列表则再循环打印出它里面的元素。
我想要的结果是：
1
3
4
alex
3
7
8
taibai
5
ritian
"""

# li = [1, 3, 4, "alex", [3, 7, 8, "TaiBai"], 5, "RiTiAn"]
# for i in li:
#     if type(i) == list:
#         for em in i:
#             if type(em) == str:
#                 print(em.lower())
#             else:
#                 print(em)
#     else:
#         if type(i) == str:
#             print(i.lower())
#         else:
#             print(i)


"""
14.用户输入一个数字,使用列表输出这个数字内的斐波那契数列,如下列表:(选做题)
用户输入100 输出[1,1,2,3,5,8,13,21,34,55,89]这个列表
"""
# lst = [1,1]
# num = input("请输入数字:")  # 10
# if num.isdecimal():
#     new_num = int(num)
#     while lst[-1] + lst[-2] <= new_num:
#         lst.append(lst[-1]+lst[-2])
#     print(lst)
# else:
#     print("请输数字!")

