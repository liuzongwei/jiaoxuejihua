# 开放封闭原则:
# 扩展是开放的(增加新功能)
# 修改源码是封闭(修改已经实现的功能)

# 在不改变源代码及调用方式的基础下额外增加新的功能

# 装饰器:用来装饰的工具

# 版一:

import time
# start_time = time.time()
# def func():
#     time.sleep(2) #睡眠 (模拟网络延时)
#     print("我要飞")
# func()
# print(time.time() - start_time)

# start_time = time.time()
# def foo():
#     time.sleep(3) #睡眠 (模拟网络延时)
#     print("我是小明,我飞的比你高")
# foo()
# print(time.time() - start_time)

# 版二:
# def times(f):
#     start_time = time.time()
#     f()
#     print(time.time() - start_time)

# def foo():
#     time.sleep(3) #睡眠 (模拟网络延时)
#     print("我是小明,我飞的比你高")

# def func():
#     time.sleep(1) #睡眠 (模拟网络延时)
#     print("我是业儿,我起不来")
# s = func
# func = times

# s = foo
# foo = times
# foo(s)

# 版三(初识版装饰器):

# def times(f):
#     def inner():
#         start_time = time.time()
#         f()
#         print(time.time() - start_time)
#     return inner
#
# def foo():
#     time.sleep(1) #睡眠 (模拟网络延时)
#     print("我是李业,我快!")
#
# foo = times(foo)
# foo()

# 版四(第二版装饰器):

# def wrapper():
#     def inner():
#         print(1)
#     return inner  # 切记不要加括号
# wrapper()


# def wrapper(f):
#     def inner():
#         print(f)
#     return inner  # 切记不要加括号
# wrapper("alex")

# def wrapper(f):
#     def inner():
#         f()
#     return inner  # 切记不要加括号
#
# def func():
#     print("这是func函数,李业还是不行")
# func = wrapper(func)
# func()

# def wrapper(f):
#     def inner(*args,**kwargs):
#         f(*args,**kwargs) # func("alex")
#     return inner  # 切记不要加括号
#
#
# def func(*args,**kwargs):
#     print(f"这是{args}函数,李业还是不行")
#
#
# func = wrapper(func)
# func("alex","sur")

# low版
# import time
# def wrapper(f):
#     def inner(*args,**kwargs):
#         "被装饰前"
#         start_time = time.time()
#         f(*args,**kwargs) # func("alex")
#         print(time.time() - start_time)
#         "被装饰后"
#     return inner  # 切记不要加括号
#
#
# def func(*args,**kwargs):
#     print(f"这是{args}函数,李业还是不行")
#     time.sleep(2) #模拟网络延时
#
#
# func = wrapper(func)
# func("alex","sur")


# 高级
# import time
# def wrapper(f):
#     def inner(*args,**kwargs):
#         "被装饰前"
#         start_time = time.time()
#         f(*args,**kwargs) # func("alex")
#         print(time.time() - start_time)
#         "被装饰后"
#     return inner  # 切记不要加括号
#
# # @wrapper  # func = wrapper(func)
# def func(*args,**kwargs):
#     print(f"这是{args}函数,李业还是不行")
#     time.sleep(2) #模拟网络延时
# # @wrapper  # foo = wrapper(foo)
# def foo(*args,**kwargs):
#     print(f"这是{args}函数,常鑫穿齐*小短裤")
#     time.sleep(3) #模拟网络延时
# func = wrapper(func)
# foo = wrapper(foo)
# func("alex","sur")
# foo("alex","sur")


# 语法糖 -- 甜
# 语法糖必须放在被装饰的函数正上方
# func = wrapper(func)
# foo = wrapper(foo)



# import time
# def wrapper(f):
#     def inner(*args,**kwargs):
#         "被装饰前"
#         start_time = time.time()
#         ret = f(*args,**kwargs) # func("alex")
#         print(time.time() - start_time)
#         "被装饰后"
#         return ret
#     return inner  # 切记不要加括号
#
# @wrapper  # func = wrapper(func)
# def func(*args,**kwargs):
#     print(f"这是{args}函数,李业还是不行")
#     time.sleep(2) #模拟网络延时
#     return "alex"
# print(func())


# login_dic = {
#     "username": None,
#     "flag": False
# }
#
#
# def auth(f):
#     def inner(*args,**kwargs):
#         if login_dic["flag"]:
#             return f()
#         else:
#             return login()
#     return inner
#
# @auth
# def index():
#     print(f"这是{login_dic['username']}主页")
#     return "主页没有内容"
#
# def login():
#     print("这是一个登录页面")
#     user = input("username:")
#     pwd = input("password:")
#     if user == "baoyuan" and pwd == "baoyuan123":
#         login_dic["flag"] = True
#         login_dic["username"] = user
#         return "登录成功!"
#
#     else:
#         return "用户名或密码错误!"
#
# @auth
# def comment():
#     print(f"这是{login_dic['username']}评论")
#
# while not login_dic["flag"]:
#     print(comment())

# 今天的作业,比如:评论 -- 登录 -- 评论 (装饰器)