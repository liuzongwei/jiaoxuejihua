# time -- 时间

import time
# print(time.time())  # 时间戳 浮点数
# print(time.time() + 5000000000)  # 时间戳 浮点数
# time.sleep(3) #  睡眠         # 秒

# print(time.strftime("%Y-%m-%d %H:%M:%S"))  # 给人看的

# print(time.gmtime())  # 结构化时间 数据类型是是命名元组
# print(time.gmtime()[0])
# print(time.gmtime().tm_year)


# 将时间戳转换成字符串时间
# print(time.strftime("%Y-%m-%d %H:%M:%S",time.gmtime(1564028611.631374)))

# 将字符串时间转换成时间戳
# print(time.mktime(time.strptime("2024-3-16 12:30:30","%Y-%m-%d %H:%M:%S")))

# time重点:
# time.time()
# time.sleep()
# time.gmtime() / time.localtime() #
# time.strftime("格式化","结构化时间") #
# time.strptime("字符串","格式化")
# time.mktime()

# "2019-10-14 17:30:20"  练习题
