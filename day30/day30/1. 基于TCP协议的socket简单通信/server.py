import socket

phone = socket.socket()

phone.bind(('127.0.0.1',8848))

phone.listen()
# listen: 允许5个人链接我,剩下的链接也可以链接,等待.

conn,addr = phone.accept()  # 等待客户端链接我,阻塞状态中
print(conn)
print(addr)

from_client_data = conn.recv(1024)  # 最多接受1024字节
print(f'来自客户端{addr}消息:{from_client_data.decode("utf-8")}')

to_client_data = input('>>>').strip().encode('utf-8')
conn.send(to_client_data)
conn.close()
phone.close()


