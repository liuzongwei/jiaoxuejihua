# -*- coding:utf-8 -*-
# range -- 范围
# print(list(range(0,10)))  # 顾头不顾尾

# python3 中的range是一个可迭代对象 写得是怎样的打印的时候就是怎样
# 获取成列表:list(range(0,10)) 顾头不顾尾
# python2 中的range返回的是一个列表

# print(list(range(0,10,1))) #步长不写得时候默认为1  [0:10:1]
# print(list(range(10,-2,-1))) #步长不写得时候默认为1  [10:-2:-1]
# print(list(range(10))) #步长不写得时候默认为1  [:10]

# 使用for循环和range 打印 100 ~ 1
# for i in range(100,0,-1):
#     print(i)

# for i in range(1,101,2):
#     print(i)

# for i in range(0,101,2):
#     print(i)

# for i in range(0,101,6):
#     print(i)


# 错误示例:
# while range(0,10):
#     print(1)

# lst = []
# flage = True
# while flage:
#     name = input("请输姓名:")
#     lst.append(name)
#     if len(lst) == 3:
#         flage = False
#
# print(lst)


# lst = []
# for i in range(3):
#     lst.append(input("请输入姓名:"))
# print(lst)