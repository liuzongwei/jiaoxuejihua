# 1.包
    # import 包.包.包
    # from 包.包.包 import 模块
    # 路径:
    #     绝对:从最外层的包开始导入
    #     相对:从当前(.)开始导入或者父级(..)导入
    # 使用相对路径的时候必须在包的外层且同级

    # from 包 import *
    # 需要在__init__.py做操作

    # python2: import 文件夹(没有__init__.py)报错
    # python3: import 文件夹(没有__init__.py)不报错

    # 包:自己一定要多练练

# 2.日志:
#     记住怎么使用就好了

#     自己定义日志开始
#     import logging
#     logger = logging.getLogger()
#     # 创建一个logger
#     fh = logging.FileHandler('test.log',mode="a",encoding='utf-8')   # 文件
#     ch = logging.StreamHandler()   # 屏幕
#     formatter = logging.Formatter('%(asctime)s - %(name)s - %(filename)s - [line:%(lineno)d] -  %(levelname)s - %(message)s')
#     # 将屏幕和文件都是用以上格式
#     logger.setLevel(logging.DEBUG)
#     # 设置记录级别
#     fh.setFormatter(formatter)
#     # 使用自定义的格式化内容
#     ch.setFormatter(formatter)
#     logger.addHandler(fh) #logger对象可以添加多个fh和ch对象
#     logger.addHandler(ch)
#     自己定义日志结束
#
#     logger.debug('logger debug message')
#     logger.info('logger info message')
#     logger.warning('logger warning message')
#     logger.error('logger error message')
#     logger.critical('logger critical message')