# import bake
# bake.api.policy.func()

# import bake.api.policy
# bake.api.policy.func()


# import bake.api.policy as p
# import bake.db.models as s
# p.func()
# s.foo()

# from bake.api.policy import func
# func()

# from bake.api import *
# www.ww()

# from bake.cmd import *
# manage.get()

# from bake.db import models
# models.foo()

# from ss.bake import *
# db.models.foo()
# api.policy.func()
# cmd.manage.get()

# import ss
# ss.bake.api.policy.func()

# import sys
# print(sys.path)
# from ss.bake import *

# import www
# www.ww()
# api.www.ww()
# print(sys.path)

# 两种:
    # 绝对路径导入:
        # from ss.bake.api.policy import func
        # func()
    # 相对路径导入:
        # from .. api.www import ww
        # ww()

    # 注意点:
        # 1.使用相对路径必须在最外层包的同级进行导入
        # 2.python2中import包,如果包没有__init__.py就报错,python3 import包,包里没有__init__.py不会报错

# from ss.bake.cmd.manage import get
# get()

# from ss import *
# manage.get()
# policy.func()

# from ss import *
# # print(locals())
# c = bake.db.models
# c.foo()

