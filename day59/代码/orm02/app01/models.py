from django.db import models

# Create your models here.


class UserInfo(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=10)
    bday = models.DateField()
    checked = models.BooleanField()
    # now = models.DateTimeField(null=True)
    now = models.DateTimeField(auto_now_add=True,null=True)
    # 更新记录时,自动添加更新时的时间,创建新纪录时也会帮你自动添加创建时的时间
    now2 = models.DateTimeField(auto_now=True,null=True)


    def __str__(self):
        return self.name

# 1  翻译成sql语句
# 2  django内置的一个sqlite客户端将sql语句发给sqlite服务端
# 3  服务端拿到sql,到磁盘里面进行了数据操作(create table app01_userinfo(id name...))






