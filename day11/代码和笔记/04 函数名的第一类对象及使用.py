# 第一类对象 -- 特殊点

# def func():
#     print(1)
# # print(func)
# a = func
# a()



# def func():
#     print(1)
#
# lst = [func,func,func]
# for i in lst:
#     i()



# def func(f):
#     f()
# def foo():
#     print(123)
# func(foo)



# def func():
#     def foo():
#         print(123)
#     return foo
# a = func()
# a()

# 1.可以当做值被赋值给变量
# 2.当做元素存放在容器中
# 3.函数名可以当做函数的参数

# def func(f):
#     f()
#
# def foo():
#     print("is foo")
#
# func(foo)

# 4.函数名可以当做函数的返回值

# def f1():
#     def f2():
#         print(1)
#         def f3():
#             print(2)
#         return f2
#     ret = f2()  # f2 函数的内存地址
#     return ret  #
# print(f1())

