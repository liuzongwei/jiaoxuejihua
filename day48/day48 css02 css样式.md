

# 昨日内容回顾

css 层叠样式表

## css引入

```
内联:
	<div style='color:red;'></div>
内部:
	head标签中的style标签中写
外部文件引入
	<link rel='stylesheet' href='css文件路径'>

```

## 选择器

### 基础选择器

```
元素选择器
类选择器
id选择器
```

### 组合选择器

```
后代选择器 div p
儿子选择器 div>p
毗邻选择器 div+p
弟弟选择器 div~p
组合选择器 div,p
div.c1{xx:xx};  过滤,找到有c1类值的div标签
div .c1  找后代,找到div标签后代中有类值为c1的所有标签
示例
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

        div .c1{
            color:red;
        }

    </style>
</head>
<body>


<div>div1
    <span class="c1">span1</span>
</div>

<div class="c1">div2</div>
<div class="c1">div3</div>

</body>
</html>
```

### 属性选择器

```
<div xxx='ooo'></div>
[xxx]{}
[xxx='ooo']{}
div[xxx]
div[xxx='ooo']
```



### 伪类选择器

```
a:link{}  
a:active{}
a:visited{}
a:hover{}
input:focus{}
```

### 伪元素选择器

```
first-letter
before
after
```

继承

优先级(权重)

```
div{color:red!important;} 最高级别
```

### 通用选择器

```
*{color:red;}
```



# 今日内容

## css样式

### 高度宽度

```
width宽度
height高度
	块级标签能设置高度宽度,内联标签不能设置高度宽度,内敛标签的高度宽度由标签内部的内容来决定.
示例
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>

        div{
            height: 100px;
            width: 200px;
            background-color: purple;
        }
        span{
            height: 100px;
            width: 200px;
            background-color: yellow;
        }

    </style>
</head>
<body>
<div>div1</div>
<span>span1</span>


</body>
</html>
```

### 字体属性

```
字体
font-family:'宋体','楷体'...
字体大小
font-family: '楷体','黑体';  /* 字体,从左往右找浏览器能够支持的字体 */
font-size: 10px;           /* 设置字体大小,默认字体大小是16px */
字重
font-weight:bold;加粗

字体颜色
/*color:red;*/
/*color: #668B8B;  */
/*color: rgb(255, 170, 205);*/

```

字重设置的值

　　　　　　font-weight用来设置字体的字重（粗细）。

|   值    |                      描述                      |
| :-----: | :--------------------------------------------: |
| normal  |                默认值，标准粗细                |
|  bold   |                      粗体                      |
| bolder  |                      更粗                      |
| lighter |                      更细                      |
| 100~900 | 设置具体粗细，400等同于normal，而700等同于bold |
| inherit |             继承父元素字体的粗细值             |



### 文字属性

#### 文字对齐(水平方向对齐)

```
text-align: center;
text-align: right;
text-align: left;
```

text-align 属性规定元素中的文本的水平对齐方式。（letter-spacing）

|   值   |      描述       |
| :----: | :-------------: |
|  left  | 左边对齐 默认值 |
| right  |     右对齐      |
| center |    居中对齐     |
|        |                 |

垂直对齐

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div{
            width: 200px;
            height: 200px;
            border: 1px solid red;
            text-align: center;
            line-height:200px;
        }
    </style>
</head>
<body>

<div>
    <span>
        xxxxx
    </span>

</div>

</body>
</html>
```

#### 文字装饰

```
text-decoration: none;
text-decoration: overline;

<a href="">百度</a>
示例
a{
	text-decoration: none;
}
```

text-decoration 属性用来给文字添加特殊效果。

|      值      |                 描述                  |
| :----------: | :-----------------------------------: |
|     none     |        默认。定义标准的文本。         |
|  underline   |         定义文本下的一条线。          |
|   overline   |         定义文本上的一条线。          |
| line-through |       定义穿过文本下的一条线。        |
|   inherit    | 继承父元素的text-decoration属性的值。 |



#### 首行缩进

```
p{
	text-indent: 32px;
}
示例
<p>
    唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.唧唧复唧唧,木兰当户织.
</p>
```



### 背景属性

```
/*background-color: blue;*/ /* 设置背景颜色 */
background-image: url("meinv.jpg");  /* 背景图片,url属性值为图片路径 */
background-repeat: no-repeat; /* 图片是否平铺,默认是平铺的,占满整个标签 */
/*background-position: right bottom; !* 图片位置 *!*/
/*background-position: 100px 50px; !* 图片位置,100px是距离左边的距离,50px是距离上面的距离 *!*/


```



![1568168771517](C:\Users\Administrator\Desktop\assets\1568168771517.png)

简写方式

```

background: yellow url("meinv.jpg") no-repeat 100px 50px;
背景颜色 背景图片路径 是否平铺 图片位置
```



### 边框属性

```

/*border-style: dotted;*/  样式
/*border-color: red;*/   颜色
/*border-width: 10px;*/  宽度
简写形式:
	/*border: 10px solid yellow;*/

四个边框可以单独设置:
	border-left:10px solid yellow ;
	border-right:10px dashed red ;
设置圆角
	border-radius: 5%;  /* 设置圆角 */

示例:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div{
            /*border-style: dotted;*/
            /*border-color: red;*/
            /*border-width: 10px;*/
            /*border: 10px solid yellow;*/
            border-left:10px solid yellow ;
            border-right:10px dashed red ;
            border-radius: 5%; 
            width: 200px;
            height: 200px;
        }
    </style>
</head>
<body>
<div></div>
</body>
</html>

```

边框样式的值

|   值   |      描述      |
| :----: | :------------: |
|  none  |    无边框。    |
| dotted | 点状虚线边框。 |
| dashed | 矩形虚线边框。 |
| solid  |   实线边框。   |



display

| 值                     | 意义                                                         |
| ---------------------- | ------------------------------------------------------------ |
| display:"none"         | HTML文档中元素存在，但是在浏览器中不显示。一般用于配合JavaScript代码使用。 |
| display:"block"        | 默认占满整个页面宽度，如果设置了指定宽度，则会用margin填充剩下的部分。 |
| display:"inline"       | 按行内元素显示，此时再设置元素的width、height、margin-top、margin-bottom和float属性都不会有什么影响。 |
| display:"inline-block" | 使元素同时具有行内元素和块级元素的特点。                     |



```
 示例
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>

        div{
            height: 200px;
            width: 200px;
            border: 1px solid red;
            /*display: inline;*/
            /*display: inline-block;*/
            display: none; /* 隐藏标签 */
        }

        span{
            height: 200px;
            width: 200px;
            border: 1px solid green;
            /*display: block;*/

        }
        p{
            height: 200px;
            width: 200px;
            border: 1px solid yellow;
            display: inline;

        }

    </style>
</head>
<body>
<div>xxxxxxx</div>
<span>span1111</span>
</body>
</html>

```



隐藏标签

```
/*display: none;*/   /* 隐藏标签,不占原来的位置 */
visibility: hidden;  /* 原来的位置还占着 */
```



### 盒子模型

```
content:内容  width和height 是内容的高度宽度
padding:内边距 内容和边框之间的距离
border:边框
margin:外边距 标签之间的距离,如果两个标签都设置了margin,选最大的值,作为双方之间的距离
占用空间大小:content+padding+border
```

```
示例
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .c1{
            width: 100px;
            height: 100px;
            border: 10px solid red;
            /*padding: 20px 20px; !* 内边距,内容和边框之间的距离 *!*/
            padding: 8px 2px 3px 6px; /* 上右下左 */
            margin: 20px 10px;

        }
        .c2{
            width: 100px;
            height: 100px;
            border: 10px solid green;
            margin: 10px 2px 6px 8px; /* 外边距,与其他标签的距离,如果旁边没有标签,按照父级标签的位置进行移动 */
        }
        .c3{
            width: 100px;
            height: 100px;
            border: 1px solid blue;
        }
    </style>
</head>
<body>
<div class="c1">
    div1
</div>
<div class="c2">
    div2
</div>
<div class="c3">
    div3
</div>
</body>
</html>
```

四个方向单独设置padding

```
padding-left: 10px;
padding-top: 8px;
padding-right: 5px;
padding-bottom: 5px;
```

四个方向单独设置margin

```
margin-top: 10px;
margin-left: 100px;
margin-bottom: 50px;
margin-right: 200px;
```





### float浮动

一般用来进行页面布局

```
浮动会脱离正常文档流
会造成父级标签塌陷问题
```



清除浮动(解决塌陷问题)

```
clear: both; /* clear清除浮动 */ left\right

方式1:
	1.父级标签设置高度
	2.通过伪元素选择器来进行清楚浮动:写法如下
		.clearfix:after{
            content:'';
            display: block;
            clear: both;
        }
     示例:
     	<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <style>

        .c1{
            width: 100px;
            height: 100px;
            background-color: red;
            float: left;
        }
        .c2{
            width: 100px;
            height: 100px;
            background-color: green;
            float: right;
        }
        .c3{
            /*width: 100px;*/
            height: 100px;
            background-color: pink;
            /*clear: both; !* clear清除浮动 *!*/
        }

        .clearfix:after{
            content:'';
            display: block;
            clear: both;
        }

        /* 浮动,会造成父级标签塌陷问题 */
        /* 解决父级标签塌陷问题方式1 */
        /*.cc{*/
        /*    height: 100px;*/
        /*}*/
    </style>

</head>
<body>

<!--ajsdfja;sdjfo;asjdfo-->
<div class="cc clearfix">
    <div class="c1">div1</div>
    <div class="c2">div2</div>
</div>
<div class="c3">div3</div>

</body>
</html>
	
```



clear清楚浮动

## 　　clear

　　　　clear属性规定元素的哪一侧不允许其他浮动元素。

| 值      | 描述                                  |
| ------- | ------------------------------------- |
| left    | 在左侧不允许浮动元素。                |
| right   | 在右侧不允许浮动元素。                |
| both    | 在左右两侧均不允许浮动元素。          |
| none    | 默认值。允许浮动元素出现在两侧。      |
| inherit | 规定应该从父元素继承 clear 属性的值。 |



overflow溢出

| 值      | 描述                                                     |
| ------- | -------------------------------------------------------- |
| visible | 默认值。内容不会被修剪，会呈现在元素框之外。             |
| hidden  | 内容会被修剪，并且其余内容是不可见的。                   |
| scroll  | 内容会被修剪，但是浏览器会显示滚动条以便查看其余的内容。 |
| auto    | 如果内容被修剪，则浏览器会显示滚动条以便查看其余的内容。 |
| inherit | 规定应该从父元素继承 overflow 属性的值。                 |

















































































