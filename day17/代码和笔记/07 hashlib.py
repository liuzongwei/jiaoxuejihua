# hashlib 加密
# 加密和校验

# alex:alex123
# alex:23lw23jky321jh4gqyt1234gj8b7t  # 加密后
# {"1234":23lw23jky321jh4gqyt1234gj8b7t}

# md5,sha1,sha256,sha512
# 1.只要明文相同密文就是相同的
# 2.只要明文不相同密文就是不相同的
# 3.不能反逆(不能解密) -- md5中国人破解了

# alex:alex123
# alex:b75bd008d5fecb1f50cf026532e8ae67

# print(len("b75bd008d5fecb1f50cf026532e8ae67"))

# 加密:
#     1.加密的内容
#     2.将要加密的内容转成字节

# import hashlib
# md5 = hashlib.md5()
# md5.update("alex123".encode("utf-8"))
# print(md5.hexdigest())
#
# md5 = hashlib.md5()
# md5.update("alex".encode("utf-8"))
# print(md5.hexdigest())

# md5 = hashlib.md5()
# md5.update("alex123".encode("gbk"))
# print(md5.hexdigest())

# md5 = hashlib.md5()
# md5.update("alex123".encode("shift-jis"))
# print(md5.hexdigest())

# import hashlib
# sha1 = hashlib.sha256()
# sha1.update("alex123".encode("utf-8"))
# print(sha1.hexdigest())

# md5 = hashlib.md5()
# md5.update("alex123".encode("utf-8"))
# print(md5.hexdigest())

# 最常用是的md5,平时加密的时候使用sha1

# 加盐

# 加固定盐
# import hashlib
# md5 = hashlib.md5("常鑫".encode("utf-8"))
# md5.update("alex123".encode("utf-8"))
# print(md5.hexdigest())
#
# md5 = hashlib.md5()
# md5.update("alex123".encode("utf-8"))
# print(md5.hexdigest())


# 动态加盐

# user = input("username:")
# pwd = input("password")
#
# import hashlib
# md5 = hashlib.md5(user.encode("utf-8"))
# md5.update(pwd.encode("utf-8"))
# print(md5.hexdigest())

# md5 = hashlib.md5()
# md5.update(pwd.encode("utf-8"))
# print(md5.hexdigest())

# 767db14ed07b245e24e10785f9d28e29

# f = open(r"F:\s24\day17\python-3.6.6-amd64.exe","rb")
# import hashlib
# md5 = hashlib.md5()
# md5.update(f.read())
# print(md5.hexdigest())

# ss = "baoyuanalextaibai"
# s = "baoyuan"
# s1 = "alex"
# s2 = "taibai"
# import hashlib
# md5 = hashlib.md5()
# md5.update(ss.encode("utf-8"))
# print(md5.hexdigest())
#
#
# md5 = hashlib.md5()
# md5.update(s.encode("utf-8"))
# md5.update(s1.encode("utf-8"))
# md5.update(s2.encode("utf-8"))
# print(md5.hexdigest())

# 节省内存

# f = open(r"F:\s24\day17\python-3.6.6-amd64.exe","rb")
# import hashlib
# md5 = hashlib.md5()
# while True:
#     msg = f.read(1024)
#     if msg:
#         md5.update(msg)
#     else:
#         print(md5.hexdigest())
#         break

