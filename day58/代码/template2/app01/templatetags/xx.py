
from django import template

register = template.Library()  #register名字不能变   注册器


# @register.filter
# def addoo(n1):
#     '''
#     无参数的过滤器
#     :param n1:  变量的值 管道前面的
#     :param n2:  传的参数 管道后面的,如果不需要传参,就不要添加这个参数
#     :return:
#     '''
#
#     return n1+'oo'


@register.filter
def addoo(n1,n2):
    '''
    有参数的过滤器
    :param n1:  变量的值 管道前面的
    :param n2:  传的参数 管道后面的,如果不需要传参,就不要添加这个参数
    :return:
    '''
    return n1+n2

# 自定义过滤器,参数至多有两个


# 自定义标签
@register.simple_tag
def huxtag(n1,n2):  #冯强xx  '牛欢喜'
    '''
    自定义标签没有参数个数限制
    :param n1:  变量的值 管道前面的
    :param n2:  传的参数 管道后面的,如果不需要传参,就不要添加这个参数
    :return:
    '''
    return n1+n2

# inclusion_tag 返回html片段的标签
@register.inclusion_tag('result.html')
def res(n1): #n1 : ['aa','bb','cc']

    return {'li':n1 }









