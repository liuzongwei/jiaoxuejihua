# join让主进程等待子进程结束之后,在执行主进程.
# from multiprocessing import Process
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(2)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#
#     p = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p.start()
#     p.join()
#     print('==主开始')


# 多个子进程使用join

# from multiprocessing import Process
# import time
#
# def task(name,sec):
#     print(f'{name}is running')
#     time.sleep(sec)
#     print(f'{name} is gone')
#
#
# if __name__ == '__main__':
#     start_time = time.time()
#     p1 = Process(target=task,args=('常鑫',1))
#     p2 = Process(target=task,args=('李业',2))
#     p3 = Process(target=task,args=('海狗',3))
#     p1.start()
#     p2.start()
#     p3.start()
#     print(f'==主{time.time()-start_time}')  # 0.02 这只是主进程结束的时间,与其他进程毫无关系


# 验证1

# from multiprocessing import Process
# import time
#
# def task(name,sec):
#     print(f'{name}is running')
#     time.sleep(sec)
#     print(f'{name} is gone')
#
#
# if __name__ == '__main__':
#     start_time = time.time()
#     p1 = Process(target=task,args=('常鑫',1))
#     p2 = Process(target=task,args=('李业',2))
#     p3 = Process(target=task,args=('海狗',3))
#
#     p1.start()
#     p2.start()
#     p3.start()
#     # join 只针对主进程,如果join下面多次join 他是不阻塞的.
#     p1.join()
#     p2.join()
#     p3.join()
#
#     print(f'==主{time.time()-start_time}')

#验证2

# from multiprocessing import Process
# import time
#
# def task(name,sec):
#     print(f'{name}is running')
#     time.sleep(sec)
#     print(f'{name} is gone')
#
#
# if __name__ == '__main__':
#     start_time = time.time()
#     p1 = Process(target=task,args=('常鑫',3))
#     p2 = Process(target=task,args=('李业',2))
#     p3 = Process(target=task,args=('海狗',1))
#
#     p1.start()
#     p2.start()
#     p3.start()
#     # join就是阻塞
#
#     p1.join()  # 等2s
#     print(f'==主1:{time.time()-start_time}')
#     p2.join()
#     print(f'===主2:{time.time()-start_time}')
#     p3.join()
#     print(f'==主3:{time.time()-start_time}')  #


#  优化上面的代码



from multiprocessing import Process
import time

def task(sec):
    print(f'is running')
    time.sleep(sec)
    print(f' is gone')


if __name__ == '__main__':
    start_time = time.time()
    # p1 = Process(target=task,args=(1,))
    # p2 = Process(target=task,args=(2,))
    # p3 = Process(target=task,args=(3,))
    #
    # p1.start()
    # p2.start()
    # p3.start()
    # # join 只针对主进程,如果join下面多次join 他是不阻塞的.
    # p1.join()
    # p2.join()
    # p3.join()



    # 错误示范:
    # for i in range(1,4):
    #     p = Process(target=task,args=(i,))
    #     p.start()
    #     p.join()
    # '''
    # p1 = Process(target=task,args=(1,))
    # p1.start()
    # p1.join()
    # p2 = Process(target=task,args=(2,))
    # p2.start()
    # p2.join()
    # p3 = Process(target=task,args=(3,))
    # p3.start()
    # p3.join()
    #
    # '''

    # 正确示范:
    # l1 = []
    # for i in range(1, 4):
    #     p = Process(target=task,args=(i,))
    #     l1.append(p)
    #     p.start()
    #
    # for i in l1:
    #     i.join()
    #
    # print(f'==主{time.time()-start_time}')

    # join就是阻塞,主进程有join,主进程下面的代码一律不执行,直到进程执行完毕之后,在执行.


